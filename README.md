# TopologyController

## Introduction

The main goal of this controller is to create and update the Kubernetes `Endpoints` objects paired to the Kubernetes _Service_ objects created by Kumori's [deployment controller](https://gitlab.com/kumori-systems/community/controllers/deployment) and representing _V3Deployment_ objects connectors.

## Project structure

```
.
├── artifacts              // Some controller samples
├── bin                    // The controller binary
├── hack                   // Some utility tools like, for example, a script to run Kubernetes code-generator.
├── cmd                    // The project apps. Currently `controller-manager` is the only one.
│   └── controller-manager // Manages the controller included in this project.
├── go.mod                 // Dependencies
├── pkg
│   ├── controllers        // Contains one folder per controller.
│   │   └──  topology      // Listens for events related with V3Deployments PODs
│   └── utils              // Utility functions and structures
│       ├── kube           // Tools to manage kube objects.
│       │   └── endpoints  // Tools to manage Endpoints kube objects.
│       └── utils.go       // Generic Tools.
├── Dockerfile             // Used to generate this controller images
├── Makefile               // Used to compile, generate images and so on
└── README.md
```

## cmd/controller-manager

The `cmd` contains the project main function of the TopologyController's controller manager. In essence, it gets the cluster configuration and creates one instance of `pkg/controllers/topology` controller.

The controller manager accepts four parameters:

* kubeconfig (optional): rute to the cluster configuration file. It must be used if
  the controller manager is not executed as a Kubernetes application and you don't want
  to use the default configuration file (if any).
* ns (mandatory): namespace of the objects managed by this controller. The controller
  execution will be aborted if this parameter is missing.
* label (optional): if set, only the objects containing the labels will be managed.
* resync (optional): time the informers will wait between resynchronizations with the cluster.

Example:

```
$ ./bin/manager-ns=kumori -kubeconfig=$PWD/config/kubeconfig -label="name=value"
```

## Topology Controller Design

This controller has been constructed on the top of [Base Controller](https://gitlab.com/kumori-systems/community/libraries/base-controller). This controller has not any explicit primary object type but it enqueues _Service_ objects created by the [Kmv3Controller] to access the connectors targets. The controller logic is fired each time a _POD_, _Service_, _Endpoint_, _V3Deployment_ and _KukuLink_ event shows up related to a given _V3Deployment_. When that happens, the Topology Controller runs the following high abstraction algorithm:

* Get and enqueue the affected connector _Service_ objects.
* Whan a _Service_ is popped from the queue, get the PODs representing instances of roles connected through a _server_ or _duplex_ channel to that _Service_ connector.
* If needed, create or update the corresponding _Endpoint_ objects adding one _Address_ per target POD and channel. The _Service_ objects are used to keep updated the DNS such that a connector name can be used to reach that connector reachable PODs acting as servers.

WARNING: The connector should take _KukuLinks_ into account to identify the affected connectors and calculat the reachable PODs. However, **that is not implemented in the current version**.

That algoritm can be detailed in the following steps:

* When an event is fired:
  * If the source object is a _V3Deployment_, find the _Service_ objects representing the _V3Deployment_ connectors (one per full connector and two per load balancer connector). Pending: enqueue also connector from other _V3Deployment_ linked to this one through a server or duplex channel.
  * If the source is a _Service_, enqueue the _Service_ if it belongs to a _V3Deployment_ connector (has a `service` and `connector` labels).
  * If it is an _Endpoint_ object, get the related _Service_ object and enqueue it if the _Endpoint_ object has a `service` label.
  * If it is a _Pod_ object with a `service` label, get all the _Service_ objects representing connectors linked to this _Pod_ server or duplex channels.
  * If it is a _KukuLink_, get the connected _V3Deployment_ object and enqueue its connectors.
* When a _Service_ is popped from the queue
  * Get the _Service_ context. That is, the Kumori and Kubernetes objects related to that _Service_.
    * Recover the _Service_ manifest.
    * Recover the related _V3Deployment_ manifest.
    * Recover the manifests of the _Pods_ connected to that _Service_ connector through a server or duplex channels.
  * Create the expected _Endpoints_ object. Those endpoints will contain:
    * An `Subset` per role not scaled to 0 and reachable through this connector to a server or duplex channel. For each `Subset` there will be:
      * An `Address` per reachable _Pod_ through a server or duplex channel. If that _Pod_ is status is `Ready`, it is added to the `Addresses` array. If it isnt, it is added to the `NotReadyAddresses` array.
      * A `Port` per connected server or duplex channel.
  * Update the cluster state with:
    * Add any expected _Endpoint_ not found in the current cluster state.
    * Update any expected _Endpoint_ found also in the current cluster state but with relevant changes, which are:
      * The `OwnerReferences` change.
      * The metadata `Labels` change.
      * The `Subsets` has different addresses and ports. The order is not considered.
    * The _Endpoints_ objects are removed automatically by Kubernetes controller when the paired _Service_ is removed.

WARNING: Links are not currently taken into account.

For example, assume a V3Deployment representing an instance of the [calculator]() sample application. This application has the following topology:

```
  (80)           (8080)   ------------                        (8080)     ---------- (8081)
service        entrypoint |          | restapiclient       restapiserver |        | hello
---------<LB>-------------| frontend |---------------<LB>----------------| worker |--------<FULL>
    serviconnector        |          |           lbconnector             |        |     fullconnector
                          ------------                                   ----------
```

And the Topology Controller receives an event from the following _Pod_:

```yaml
apiVersion: v1
kind: Pod
metadata:
  ...
  labels:
    app: kd-142954-e03d8a3f-worker
    controller: deploymentcontroller
    controller-revision-hash: kd-142954-e03d8a3f-worker-deployment-654cd5777
    kumoriOwner: jbgisbert__arroba__kumori.systems
    owner: kd-142954-e03d8a3f
    role: worker
    service: kd-142954-e03d8a3f
    statefulset.kubernetes.io/pod-name: kd-142954-e03d8a3f-worker-deployment-0
  name: kd-142954-e03d8a3f-worker-deployment-0
  namespace: kumori
spec:
  ...
status:
  ...
```

The Topology Controller looks first for the _V3Deployment_ named `kd-142954-e03d8a3f`. In this case, that deployment manifest is found:

```yaml
apiVersion: v1
items:
- apiVersion: kumori.systems/v1
  kind: V3Deployment
  metadata:
    ...
    labels:
      app: kd-142954-e03d8a3f
      domain: kumori.systems.examples
      kumoriOwner: jbgisbert__arroba__kumori.systems
      nickname: calcupatator
      version: ""
    name: kd-142954-e03d8a3f
    namespace: kumori
  spec:
    description:
      ...
      service:
        description:
          ...
          srv:
            client: {}
            server:
              service:
                port: 80
                protocol: http
          connector:
            fullconnector: full
            lbconnector: lb
            serviceconnector: lb
          link:
            frontend:
              restapiclient:
                to: lbconnector
            fullconnector:
              to:
                worker: hello
            lbconnector:
              to:
                worker: restapiserver
            self:
              service:
                to: serviceconnector
            serviceconnector:
              to:
                frontend: entrypoint
          role:
            frontend:
              ...
            worker:
              component:
                description:
                  ...
                  srv:
                    ...
                    duplex:
                      hello:
                        port: 8081
                        protocol: tcp
                    server:
                      restapiserver:
                        port: 8080
                        protocol: http
```

Then, the Topology Congtroller:

* Looks the `role` label in the POD manifest, `worker` in this case
* Looks if that role has `server` or `duplex` channels. In this case, it will find the `hello` duplex channel and the `restapiserver` server channel.
* Gets the connectors linked to those channels. In this case, the `fullconnector` is connected to `hello` and the `lbconnector` is connected to `restapiserver`. As their names say, they are a full connector and a load balancer conector respectively.
* Gets the service links linked to a client or duplex service channel declared in the POD V3Deployment. In this case, none.

The next step is to find and enqueue the _Service_ objects related to `fullconnector` and `lbconnector`, which are `fullconnector-kd-142954-e03d8a3f`, `lbconnector-kd-142954-e03d8a3f` and `lbconnector-kd-142954-e03d8a3f-lb`. For example, `fullconnector-kd-142954-e03d8a3f` will look like this:

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2020-07-29T07:03:07Z"
  labels:
    connector: fullconnector
    controller: deploymentcontroller
    kumoriOwner: jbgisbert__arroba__kumori.systems
    owner: kd-142954-e03d8a3f
    service: kd-142954-e03d8a3f
  name: fullconnector-kd-142954-e03d8a3f
  namespace: kumori
  ownerReferences:
  - apiVersion: kumori.systems/v1
    blockOwnerDeletion: true
    controller: true
    kind: V3Deployment
    name: kd-142954-e03d8a3f
    uid: 65a0a8a1-0cb0-4465-9d64-f8a7deb96552
  resourceVersion: "5526625"
  selfLink: /api/v1/namespaces/kumori/services/fullconnector-kd-142954-e03d8a3f
  uid: e7525f00-4967-4d30-afeb-d69cd9757743
spec:
  clusterIP: None
  ports:
  - name: connector0
    port: 80
    protocol: TCP
    targetPort: 80
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```

NOTE: This _Service_ does not has a `spec.selector` because the TopologyController is the one in charge of creating and filling the relared _Endpoints_ object.

At some point, the controller will pop up one of the previous _Service_ objects. Let's assume it is `fullconnector-kd-142954-e03d8a3f`.

First, the Topology Controller will recover the follwing objects:

* The `fullconnector-kd-142954-e03d8a3f` _Service_ manifest, which is already cached.
* The `kd-142954-e03d8a3f` _V3Deployment_, which is also cached.
* The `fullconnector-kd-142954-e03d8a3f` _Endpoings_ object, if already exists. Let's assume it not exists.
* All the _Pod_ objects representing the role `worker` instances, wich we assume currently are:
  * `kd-142954-e03d8a3f-worker-deployment-0`: this is the POD which fired the controller.
  * `kd-142954-e03d8a3f-worker-deployment-1`

NOTE: The _Endpoints_ object must have the same name than its related _Service_ object.

Then, the expected _Endpoints_ object is calculated. As we said, the `fullconnector` is connected through the `hello` channel (port `8081`). So, the `fullconnector-kd-142954-e03d8a3f` _Endpoints_ will look like this:

```yaml
apiVersion: v1
kind: Endpoints
metadata:
  labels:
    connector: fullconnector
    kumoriOwner: jbgisbert__arroba__kumori.systems
    owner: kd-142954-e03d8a3f
    service: kd-142954-e03d8a3f
    service.kubernetes.io/headless: ""
  name: fullconnector-kd-142954-e03d8a3f
  namespace: kumori
subsets:
- addresses:
  - ip: 172.17.177.240
    nodeName: bacalao-worker-2
    targetRef:
      kind: Pod
      name: kd-142954-e03d8a3f-worker-deployment-1
      namespace: kumori
      resourceVersion: "3989174"
      uid: 4351fbba-a0bc-4623-8c7e-00c83c14527b
  - ip: 172.17.240.231
    nodeName: bacalao-worker-3
    targetRef:
      kind: Pod
      name: kd-142954-e03d8a3f-worker-deployment-0
      namespace: kumori
      resourceVersion: "3989148"
      uid: 2b61dc5c-3163-46ab-b23f-dd781d0a05fc
  ports:
  - name: connector0
    port: 8081
    protocol: TCP
```

Since this _Endponints_ object not exists in the cluster state, it will be added.

Assume that, at some point, a new `worker` instance `kd-142954-e03d8a3f-worker-deployment-2` is created and the `fullconnector-kd-142954-e03d8a3f` is enqueued again. However, this time the `fullconnector-kd-142954-e03d8a3f` already exists and will be updated adding the new instance as an Address:


```yaml
apiVersion: v1
kind: Endpoints
metadata:
  labels:
    connector: fullconnector
    kumoriOwner: jbgisbert__arroba__kumori.systems
    owner: kd-142954-e03d8a3f
    service: kd-142954-e03d8a3f
    service.kubernetes.io/headless: ""
  name: fullconnector-kd-142954-e03d8a3f
  namespace: kumori
subsets:
- addresses:
  - ip: 172.17.177.240
    nodeName: bacalao-worker-2
    targetRef:
      kind: Pod
      name: kd-142954-e03d8a3f-worker-deployment-1
      namespace: kumori
      resourceVersion: "3989174"
      uid: 4351fbba-a0bc-4623-8c7e-00c83c14527b
  - ip: 172.17.240.231
    nodeName: bacalao-worker-3
    targetRef:
      kind: Pod
      name: kd-142954-e03d8a3f-worker-deployment-0
      namespace: kumori
      resourceVersion: "3989148"
      uid: 2b61dc5c-3163-46ab-b23f-dd781d0a05fc
  - ip: 172.17.177.243
    nodeName: bacalao-worker-2
    targetRef:
      kind: Pod
      name: kd-142954-e03d8a3f-worker-deployment-2
      namespace: kumori
      resourceVersion: "3989202"
      uid: 73c786e3-2903-444a-80bb-9891d252f59a
  ports:
  - name: connector0
    port: 8081
    protocol: TCP
```

WARNING: Service links support will not be included in the first version of Topology Controller.

## License

## License

Copyright 2022 Kumori Systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.