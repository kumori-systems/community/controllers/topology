#!/usr/bin/env bash

# Based (but not exactly) on:
# https://github.com/kubernetes/sample-controller/blob/master/hack/update-codegen.sh

set -o errexit
set -o nounset
set -o pipefail

SCRIPT_ROOT=$(dirname "${BASH_SOURCE[0]}")/..
CODEGEN_PKG=$GOPATH/src/k8s.io/code-generator  # <<< BE CAREFUL

# generate the code with:
# --output-base    because this script should also be able to run inside the vendor dir of
#                  k8s.io/kubernetes. The output-base is needed for the generators to output into the vendor dir
#                  instead of the $GOPATH directly. For normal projects this can be dropped.
"${CODEGEN_PKG}"/generate-groups.sh \
  all \
  kuku-controller/pkg/generated \
  kuku-controller/pkg/apis \
  kumori:v1 \
  --output-base "${SCRIPT_ROOT}/.." \
  --go-header-file "${SCRIPT_ROOT}"/hack/boilerplate.go.txt
