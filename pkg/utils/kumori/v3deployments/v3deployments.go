/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package v3Deployments

import (
	"fmt"
	"topology-controller/pkg/utils/naming"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/labels"
)

type ConnectorInfo struct {
	Connector *kumoriv1.Connector
	Name      string
	Kind      kumoriv1.ConnectorType
	AsClient  bool
	AsServer  bool
}

// Get recovers from the Kumori cluster the current version of a V3Deployment, if any
func Get(
	lister kumorilisters.V3DeploymentLister,
	deployment *kumoriv1.V3Deployment,
) (*kumoriv1.V3Deployment, error) {
	if deployment == nil {
		return nil, fmt.Errorf("V3Deployment is nil")
	}
	return GetByName(lister, deployment.GetNamespace(), deployment.GetName())
}

// GetByName gets a V3Deployment from the cluster using its name and namespace
func GetByName(
	lister kumorilisters.V3DeploymentLister,
	namespace string,
	name string,
) (*kumoriv1.V3Deployment, error) {
	v3Deployment, err := lister.V3Deployments(namespace).Get(name)
	if err != nil {
		return nil, err
	}

	if v3Deployment != nil {
		v3Deployment = v3Deployment.DeepCopy()
	}

	return v3Deployment, nil
}

// GetByNameAndDomain returns the V3Deployment with a given name and domain. The name
// and domain are the ones provided by the service provider when the V3Deployment is
// registered
func GetByRegistrationNameAndDomain(
	lister kumorilisters.V3DeploymentLister,
	namespace string,
	name string,
	domain string,
) (*kumoriv1.V3Deployment, error) {

	// Creates the labels filter to find the V3Deployment with the given name and domain
	nameHash := naming.Hash(name)
	domainHash := naming.Hash(domain)
	matchLabels := map[string]string{
		"kumori/name":   nameHash,
		"kumori/domain": domainHash,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Searches for the specified V3Deployment
	v3Deployments, err := lister.V3Deployments(namespace).List(options)

	// Returns the error if anything wrong happens
	if err != nil {
		return nil, err
	}

	// Returns an error if a V3Deployment is not found with this name and domain
	if len(v3Deployments) == 0 {
		return nil, fmt.Errorf("Not found a V3Deployment with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns an error if more than one V3Deployment is found with this name and domain
	if len(v3Deployments) > 1 {
		return nil, fmt.Errorf("Found more than one V3Deployment with name '%s' ('%s') and domain '%s' ('%s') in namespace '%s'", name, nameHash, domain, domainHash, namespace)
	}

	// Returns the found V3Deployment
	return v3Deployments[0].DeepCopy(), nil
}

// GetChannelConnector returns the connector connecting a given channel and if it
// is connected in the clients list or the servers list
func GetChannelConnector(
	v3Deployment *kumoriv1.V3Deployment,
	channel string,
	role string,
) *ConnectorInfo {

	// Return nil if the V3Deployment has not any connector declared
	if v3Deployment.Spec.Artifact == nil ||
		v3Deployment.Spec.Artifact.Description == nil ||
		v3Deployment.Spec.Artifact.Description.Connectors == nil ||
		len(*v3Deployment.Spec.Artifact.Description.Connectors) == 0 {
		return nil
	}

	// Checks if the specified role and channel appear connected in any connector
	for connectorName, connectorData := range *v3Deployment.Spec.Artifact.Description.Connectors {
		for _, clientData := range connectorData.Clients {
			if (clientData.Channel == channel) && (clientData.Role == role) {
				return &ConnectorInfo{
					Connector: connectorData.DeepCopy(),
					Name:      connectorName,
					Kind:      connectorData.Kind,
					AsServer:  false,
					AsClient:  true,
				}
			}
		}
		for _, serverData := range connectorData.Servers {
			for _, linkData := range serverData.Links {
				if (linkData.Channel == channel) && (linkData.Role == role) {
					return &ConnectorInfo{
						Connector: connectorData.DeepCopy(),
						Name:      connectorName,
						Kind:      connectorData.Kind,
						AsClient:  false,
						AsServer:  true,
					}
				}
			}
		}
	}

	// The role and channel is not connected in this V3Deployment
	return nil
}

// GetRoleConnectors returns information about the connectors containing links
// to a given role in a given V3Deployment
func GetRoleConnectors(
	v3Deployment *kumoriv1.V3Deployment,
	role string,
) []ConnectorInfo {

	// Return nil if the V3Deployment has not any connector declared
	if v3Deployment.Spec.Artifact == nil ||
		v3Deployment.Spec.Artifact.Description.Connectors == nil ||
		len(*v3Deployment.Spec.Artifact.Description.Connectors) == 0 {
		return nil
	}

	connectors := make([]ConnectorInfo, 0, len(*v3Deployment.Spec.Artifact.Description.Connectors))

	// Checks if the specified role appears connected for each V3Deployment connector
	for connectorName, connectorData := range *v3Deployment.Spec.Artifact.Description.Connectors {
		for _, clientData := range connectorData.Clients {
			if clientData.Role == role {
				found := false
				for _, connectorInfo := range connectors {
					if connectorInfo.Name == connectorName {
						found = true
						break
					}
				}
				if !found {
					connectors = append(connectors, ConnectorInfo{
						Connector: connectorData.DeepCopy(),
						Name:      connectorName,
						Kind:      connectorData.Kind,
						AsClient:  true,
						AsServer:  false,
					})
				}
			}
		}
		for _, serverData := range connectorData.Servers {
			for _, linkData := range serverData.Links {
				if linkData.Role == role {
					found := false
					for _, connectorInfo := range connectors {
						if connectorInfo.Name == connectorName {
							found = true
							connectorInfo.AsServer = true
							break
						}
					}
					if !found {
						connectors = append(connectors, ConnectorInfo{
							Connector: connectorData.DeepCopy(),
							Name:      connectorName,
							Kind:      connectorData.Kind,
							AsClient:  false,
							AsServer:  true,
						})
					}
				}
			}
		}
	}

	// Returns information about the connectors found
	return connectors
}
