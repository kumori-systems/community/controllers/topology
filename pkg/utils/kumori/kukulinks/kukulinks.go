/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukulinks

import (
	"fmt"

	kerrors "topology-controller/pkg/utils/errors"
	"topology-controller/pkg/utils/naming"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	labels "k8s.io/apimachinery/pkg/labels"
)

// KukuLinkTools includes functions related to kukulinks
type KukuLinkTools struct {
	v3DeploymentLister kumorilisters.V3DeploymentLister
	kukuLinksLister    kumorilisters.KukuLinkLister
}

// DeploymentEndpoint represents a given link endpoint of type deployment
type DeploymentEndpoint struct {
	Deployment *kumoriv1.V3Deployment
	Channel    string
}

const (
	// DeploymentKukuLinkEndpointType is the type of a KukuLink endpoint referencing a non-top V3Deployment
	DeploymentKukuLinkEndpointType string = "deployment"
	// SolutionKukuLinkEndpointType is the type of a KukuLink endpoint referencing a top V3Deployment
	SolutionKukuLinkEndpointType string = "solution"
	// HttpInboundKukuLinkEndpointType is the type of a KukuLink endpoint referencing a HTTP Inbound
	HttpInboundKukuLinkEndpointType string = "httpinbound"
)

// NewKukuLinkTools returns a new KukuLinkTools
func NewKukuLinkTools(
	v3DeploymentLister kumorilisters.V3DeploymentLister,
	kukuLinksLister kumorilisters.KukuLinkLister,
) (c KukuLinkTools) {
	c = KukuLinkTools{
		v3DeploymentLister: v3DeploymentLister,
		kukuLinksLister:    kukuLinksLister,
	}
	return
}

// GetLinkDeployments returns the v3Deployments and channels related to
// the kukulink.
func (k *KukuLinkTools) GetLinkDeployments(kukuLink *kumoriv1.KukuLink) (
	endpoints []DeploymentEndpoint,
	err error,
) {
	err = nil

	endpoints = []DeploymentEndpoint{}
	for _, endpoint := range kukuLink.Spec.Endpoints {
		if (endpoint.Kind != DeploymentKukuLinkEndpointType) &&
			(endpoint.Kind != SolutionKukuLinkEndpointType) {
			continue
		}

		v3Deployment, err := k.getDeployment(kukuLink.Namespace, endpoint.Domain, endpoint.Name)
		if err != nil {
			err = kerrors.NewKukuError(kerrors.Major, err)
			continue
		}

		if v3Deployment != nil {
			v3Deployment = v3Deployment.DeepCopy()
		}

		endpoints = append(endpoints, DeploymentEndpoint{
			Deployment: v3Deployment,
			Channel:    endpoint.Channel,
		})
	}

	return
}

// getLinkDeployment returns the kukuHttpInbound or kukuDeployment related to
// one part of kukulink
func (k *KukuLinkTools) getDeployment(
	namespace, depDomain, depName string,
) (
	v3Deployment *kumoriv1.V3Deployment,
	kerr error,
) {
	v3Deployment = nil
	kerr = nil

	matchLabels := map[string]string{
		"kumori/domain": naming.Hash(depDomain),
		"kumori/name":   naming.Hash(depName),
	}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := k.v3DeploymentLister.V3Deployments(namespace).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Major, err)
	} else if len(list) == 0 {
		kerr = kerrors.NewKukuError(kerrors.Fatal, fmt.Sprintf("Deployment "+depDomain+"/"+depName+" not found"))
		return
	} else if len(list) > 1 {
		kerr = kerrors.NewKukuError(kerrors.Fatal, fmt.Sprintf("There are several deployments "+depDomain+"/"+depName))
	} else {
		v3Deployment = list[0].DeepCopy()
	}

	return
}

// LinkToString creates a string using kukulink info (logging) purposes)
func (k *KukuLinkTools) LinkToString(kukuLink *kumoriv1.KukuLink) string {
	endpoint1 := kukuLink.Spec.Endpoints[0]
	endpoint2 := kukuLink.Spec.Endpoints[1]
	return kukuLink.Name + ":" +
		endpoint1.Domain + "/" + endpoint1.Name + ":" + endpoint1.Channel +
		"<=>" +
		endpoint2.Domain + "/" + endpoint2.Name + ":" + endpoint2.Channel
}

// getKukuLinks returns the KukuLinks linking a given V3Deployment
func (k *KukuLinkTools) GetKukuLinks(v3Deployment *kumoriv1.V3Deployment, channel string) (
	[]kumoriv1.KukuLink,
	error,
) {
	if v3Deployment == nil {
		kerr := kerrors.NewKukuError(kerrors.Major, fmt.Errorf("Deployment is null"))
		return nil, kerr
	}
	namespace := v3Deployment.Namespace
	kukuLinks := make([]kumoriv1.KukuLink, 0, 1)
	matchLabels1 := map[string]string{
		"deployment1": v3Deployment.GetName(),
	}
	matchLabels2 := map[string]string{
		"deployment2": v3Deployment.GetName(),
	}
	if channel != "" {
		matchLabels1["channel1"] = naming.Hash(channel)
		matchLabels2["channel2"] = naming.Hash(channel)
	}
	var kcerr *kerrors.KukuError
	for _, matchLabels := range []map[string]string{matchLabels1, matchLabels2} {
		options := labels.SelectorFromSet(matchLabels)
		if links, err := k.kukuLinksLister.KukuLinks(namespace).List(options); err == nil {
			for _, elem := range links {
				link := *elem.DeepCopy()
				kukuLinks = append(kukuLinks, link)
			}
		} else {
			message := fmt.Sprintf("Error retrieving links: %v", err)
			kcerr = kerrors.NewKukuError(kerrors.Minor, fmt.Errorf(message))
		}
	}

	// To avoid https://golang.org/doc/faq#nil_error
	if kcerr == nil {
		return kukuLinks, nil
	}
	return kukuLinks, kcerr
}

// SortByName sorts a list of links by name
func (k *KukuLinkTools) SortByName(links []kumoriv1.KukuLink) {
	name := func(l1, l2 *kumoriv1.KukuLink) bool {
		return l1.Name < l2.Name
	}

	By(name).Sort(links)
}
