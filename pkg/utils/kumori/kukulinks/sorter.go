/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukulinks

import (
	"sort"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

type By func(p1, p2 *kumoriv1.KukuLink) bool

// planetSorter joins a By function and a slice of Planets to be sorted.
type linkSorter struct {
	links []kumoriv1.KukuLink
	by    func(l1, l2 *kumoriv1.KukuLink) bool // Closure used in the Less method.
}

func (by By) Sort(links []kumoriv1.KukuLink) {
	ls := &linkSorter{
		links: links,
		by:    by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(ls)
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *linkSorter) Less(i, j int) bool {
	return s.by(&s.links[i], &s.links[j])
}

// Swap is part of sort.Interface.
func (s *linkSorter) Swap(i, j int) {
	s.links[i], s.links[j] = s.links[j], s.links[i]
}

func (s *linkSorter) Len() int {
	return len(s.links)
}
