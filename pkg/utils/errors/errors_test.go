/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
	"strings"
	"testing"
)

// Test create a KukuError from an error object
func TestFromError(t *testing.T) {
	var sev KukuErrorSeverity = Fatal
	str := "This is an error"
	kukuError := NewKukuError(sev, fmt.Errorf(str))
	if kukuError.Severity != sev || kukuError.Parent.Error() != str {
		t.Errorf("Unexpected values")
	}
}

// Test create a KukuError from an string
func TestFromString(t *testing.T) {
	var sev KukuErrorSeverity = Fatal
	str := "This is an error"
	kukuError := NewKukuError(sev, str)
	if kukuError.Severity != sev || kukuError.Parent.Error() != str {
		t.Errorf("Unexpected values")
	}
}

// Test create a KukuError from an invalid parent
func TestFromInvalid(t *testing.T) {
	var sev KukuErrorSeverity = Fatal
	kukuError := NewKukuError(sev, 123)
	if kukuError.Severity != sev || kukuError.Parent.Error() != InvalidSource {
		t.Errorf("Unexpected values")
	}
}

// Test check an error aborting the flow without retries
func TestAbortNotRetry(t *testing.T) {
	var sev KukuErrorSeverity = Fatal
	str := "This is an error"
	kukuError := NewKukuError(sev, str)
	pendingRetryErrors := false
	shouldAbort, errorToReturn := CheckErrorEx(kukuError, &pendingRetryErrors)
	if !(errorToReturn == nil && shouldAbort == true && pendingRetryErrors == false) {
		t.Errorf("Unexpected values")
	}
}

// Test check an error aborting the flow with retries
func TestAbortRetry(t *testing.T) {
	var sev KukuErrorSeverity = Major
	str := "This is an error"
	kukuError := NewKukuError(sev, str)
	pendingRetryErrors := false
	shouldAbort, errorToReturn := CheckErrorEx(kukuError, &pendingRetryErrors)
	if !(errorToReturn != nil && shouldAbort == true && pendingRetryErrors == true &&
		strings.Contains(errorToReturn.Error(), str)) {
		t.Errorf("Unexpected values")
	}
}

// Test check an error not aborting the flow
func TestNotAbort(t *testing.T) {
	var sev KukuErrorSeverity = Minor
	str := "This is an error"
	kukuError := NewKukuError(sev, str)
	pendingRetryErrors := false
	shouldAbort, errorToReturn := CheckErrorEx(kukuError, &pendingRetryErrors)
	if !(errorToReturn == nil && shouldAbort == false && pendingRetryErrors == true) {
		t.Errorf("Unexpected values")
	}
}
