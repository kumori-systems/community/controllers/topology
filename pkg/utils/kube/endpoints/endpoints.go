/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package endpoints

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	utils "topology-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new endpoint in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	endpoints *v1.Endpoints,
) (*v1.Endpoints, error) {
	if endpoints == nil {
		return nil, fmt.Errorf("Endpoints is nil")
	}
	log.Debugf("endpoints.Create. Endpoints: %s", endpoints.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "topology-controller",
	}

	return client.CoreV1().Endpoints(endpoints.GetNamespace()).Create(ctx, endpoints, options)
}

// Update is used to update an existing endpoint in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	endpoints *v1.Endpoints,
) (*v1.Endpoints, error) {
	if endpoints == nil {
		return nil, fmt.Errorf("Endpoints is nil")
	}
	log.Debugf("endpoints.Update. Endpoints %s", endpoints.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "topology-controller",
	}
	return client.CoreV1().Endpoints(endpoints.GetNamespace()).Update(ctx, endpoints, options)
}

// Delete is used to delete an existing endpoint in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	endpoints *v1.Endpoints,
) error {
	if endpoints == nil {
		return fmt.Errorf("Endpoints is nil")
	}
	log.Debugf("endpoints.Delete. Endpoints %s", endpoints.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().Endpoints(endpoints.GetNamespace()).Delete(ctx, endpoints.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a Endpoints, if any
func Get(
	lister listers.EndpointsLister,
	endpoints *v1.Endpoints,
) (*v1.Endpoints, error) {
	if endpoints == nil {
		return nil, fmt.Errorf("Endpoints is nil")
	}
	return lister.Endpoints(endpoints.GetNamespace()).Get(endpoints.GetName())
}

// CreateOrUpdate checks if the given endpoint exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.EndpointsLister,
	endpoints *v1.Endpoints,
) (*v1.Endpoints, utils.Operation, error) {
	if endpoints == nil {
		return nil, utils.NoneOperation, fmt.Errorf("Endpoints is nil")
	}
	currentEndpoints, err := Get(lister, endpoints)
	if errors.IsNotFound(err) {
		finalEndpoints, err := Create(client, endpoints)
		return finalEndpoints, utils.CreatedOperation, err
	} else if err != nil {
		return endpoints, utils.NoneOperation, err
	}
	currentEndpoints = currentEndpoints.DeepCopy()
	finalEndpoints, changed, err := Apply(client, endpoints, currentEndpoints)
	if changed {
		return finalEndpoints, utils.UpdatedOperation, err
	}

	return finalEndpoints, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of endpoints in the cluster. Returns
// the list of created endpoints, the list of updated endpoints and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.EndpointsLister,
	endpointsList []v1.Endpoints,
) (created []v1.Endpoints, updated []v1.Endpoints, errors []error) {
	listLength := len(endpointsList)
	errors = make([]error, 0, 3)
	created = make([]v1.Endpoints, 0, listLength)
	updated = make([]v1.Endpoints, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, endpoints := range endpointsList {
		newEndpoints, operation, err := CreateOrUpdate(client, lister, &endpoints)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newEndpoints)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newEndpoints)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for endpoints %s. Expected operations are %s or %s", operation, endpoints.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of endpoints and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.EndpointsLister,
	endpointsList []v1.Endpoints,
	selector labels.Selector,
	namespace string,
) (
	created []v1.Endpoints,
	updated []v1.Endpoints,
	deleted []v1.Endpoints,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, endpointsList)
	currentEndpointsList, err := List(lister, selector, namespace)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.Endpoints{}
		return
	}
	if (currentEndpointsList != nil) && (len(currentEndpointsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentEndpointsList, endpointsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.Endpoints{}
	}
	return
}

// List returns the endpoints in a given namespace matching a set of labels.
func List(
	lister listers.EndpointsLister,
	selector labels.Selector,
	namespace string,
) ([]*v1.Endpoints, error) {
	return lister.Endpoints(namespace).List(selector)
}

// Apply updates "to" endpoint with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.Endpoints,
	to *v1.Endpoints,
) (*v1.Endpoints, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From endpoints is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("From endpoints is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("endpoints.Apply. Owner References differ in %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("endpoints.Apply. Labels differ in %s", from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	// Labels change if a link labeled has disappeared in the new version
	// of the object.
	for key, currentValue := range (*to).GetLabels() {
		if currentValue != "link-in-use" {
			continue
		}
		if _, ok := (*from).Labels[key]; !ok {
			log.Debugf("endpoints.Apply. Labels differ in %s", from.GetName())
			changed = true
			delete(to.Labels, key)
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Annotations[key]; !ok || (currentValue != newValue) {
			log.Debugf("endpoints.Apply. Annotation differ in %s", from.GetName())
			changed = true
			to.Annotations[key] = newValue
		}
	}

	if len(from.Subsets) != len(to.Subsets) {
		log.Debugf("endpoints.Apply. Subsets differ\n'%v'\n'%v'\n", from.Subsets, to.Subsets)
		changed = true
		to.Subsets = from.Subsets
	} else {
		for _, subset1 := range from.Subsets {
			found := false
			for _, subset2 := range to.Subsets {
				if compareSubsets(&subset1, &subset2) {
					found = true
					break
				}
			}
			if !found {
				log.Debugf("endpoints.Apply. Subsets differ\n'%v'\n'%v'\n", from.Subsets, to.Subsets)
				changed = true
				to.Subsets = from.Subsets
				break
			}
		}
	}

	if changed {
		newEndpoints, err := Update(client, to)
		return newEndpoints, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a endpoints in a cluster if cannot be found in a list of endpoints.
// Returns true if the endpoints has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	endpoints *v1.Endpoints,
	expectedList []v1.Endpoints,
) (bool, error) {
	if endpoints == nil {
		return false, fmt.Errorf("Endpoints is nil")
	}
	for _, expectedEndpoints := range expectedList {
		if (endpoints.GetNamespace() == expectedEndpoints.GetNamespace()) &&
			(endpoints.GetName() == expectedEndpoints.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, endpoints); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all Endpoints not found in existingList.
// A Endpointsis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.Endpoints,
	expectedList []v1.Endpoints,
) (deleted []v1.Endpoints, errors []error) {
	deleted = make([]v1.Endpoints, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}

func compareSubsets(subset1 *v1.EndpointSubset, subset2 *v1.EndpointSubset) bool {

	// Nil checks
	if subset1 == nil && subset2 == nil {
		return true
	}
	if subset1 == nil && subset2 != nil {
		return false
	}
	if subset1 != nil && subset2 == nil {
		return false
	}

	// Checks ports and addresses lengths
	if len(subset1.Addresses) != len(subset2.Addresses) {
		return false
	}
	if len(subset1.Ports) != len(subset2.Ports) {
		return false
	}

	// Checks addresses contents
	for _, address1 := range subset1.Addresses {
		found := false
		for _, address2 := range subset2.Addresses {
			if apiequality.Semantic.DeepEqual(address1, address2) {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	// Checks ports contents
	for _, port1 := range subset1.Ports {
		found := false
		for _, port2 := range subset2.Ports {
			if apiequality.Semantic.DeepEqual(port1, port2) {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	// So, they are equal
	return true
}
