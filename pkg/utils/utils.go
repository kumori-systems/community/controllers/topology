/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"encoding/json"
	"io/ioutil"

	"sigs.k8s.io/yaml"
)

// Operation is a code of an operation over a NetworkPolicy
type Operation string

const (
	// CreatedOperation is the code representing a NetworkPolicy creation
	CreatedOperation Operation = "created"
	// UpdatedOperation is the code representing a NetworkPolicy updating
	UpdatedOperation Operation = "updated"
	// DeletedOperation is the code representing a NetworkPolicy deletion
	DeletedOperation Operation = "deleted"
	// NoneOperation is the code representing that nothing has been done
	NoneOperation Operation = "nothing"
)

// LoadObjectFromYAML loads an object from a yaml file
func LoadObjectFromYAML(v interface{}, p string) error {
	yamlData, err := ioutil.ReadFile(p)
	if err != nil {
		return err
	}
	jsonData, err := yaml.YAMLToJSON(yamlData)
	if err != nil {
		return err
	}
	return json.Unmarshal(jsonData, v)
}
