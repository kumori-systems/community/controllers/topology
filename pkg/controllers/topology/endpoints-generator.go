/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

import (
	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EndpointsGenerator generaters the Endpoints related to a given context
type EndpointsGenerator struct {
	Context *Context
}

const (
	// endpointsGeneratorLogPrefix is used as a prefix in log lines
	endpointsGeneratorLogPrefix = controllerName + ".EndpointsGenerator"
)

// NewEndpointsGenerator returns a generator to create Endpoints objects from a Context
func NewEndpointsGenerator(context *Context) (generator *EndpointsGenerator) {
	meth := endpointsGeneratorLogPrefix + ".getV3Deployment()"
	log.Debugf("%s Service: %s", meth, context.Service.GetName())

	return &EndpointsGenerator{
		Context: context,
	}
}

// Generate creates the expected Endpoints for a given context.
func (generator *EndpointsGenerator) Generate() (*v1.Endpoints, error) {
	meth := endpointsGeneratorLogPrefix + ".Generate()"
	service := generator.Context.Service
	serviceName := service.GetName()
	log.Debugf("%s Service: %s", meth, serviceName)

	// Generates the Kubernetes Endpoints objects
	endpoints, err := generator.createServiceEndpoints()
	if err != nil {
		log.Errorf("%s. Error. Service: %s. Error: %s", meth, serviceName, err.Error())
		return nil, err
	}

	// content, _ := json.MarshalIndent(endpoints, "", "  ")
	// fmt.Printf("\n\n------->ENDPOINTS: %s", string(content))

	return endpoints, nil
}

// createServiceEndpoints creates the Endpoints object for a given service.
func (generator *EndpointsGenerator) createServiceEndpoints() (*v1.Endpoints, error) {
	meth := endpointsGeneratorLogPrefix + ".createServiceEndpoints()"
	service := generator.Context.Service
	endpointsContext := generator.Context.Endpoints
	name := service.GetName()
	log.Debugf("%s Service: %s", meth, name)

	namespace := service.GetNamespace()
	labels := service.Labels
	annotations := service.Annotations
	subsets := make([]v1.EndpointSubset, 0, len(endpointsContext))

	// Create a subset per role. A subset contains the PODs IPs and the channels ports.
	for _, context := range endpointsContext {
		addresses := make([]v1.EndpointAddress, 0, len(context.Pods))
		notReadyAddresses := make([]v1.EndpointAddress, 0, len(context.Pods))

		// Creates an address per POD. Currently, we assume IPv4.
		for _, pod := range context.Pods {
			address := createEndpointAddress(pod)
			if pod.DeletionTimestamp != nil {
				log.Debugf("%s Service: %s Pod %s is being deleted", meth, name, pod.GetName())
				continue
			}
			toBeAdded := true
			if !generator.Context.PublishNonReady {
				if pod.Status.Phase != v1.PodRunning {
					log.Debugf("%s Service: %s Pod %s not ready", meth, name, pod.GetName())
					// Empty IPs are forbidden in Endpoints. So, we add the POD to the not
					// ready list only if it already has an assigned IP
					if address.IP != "" {
						notReadyAddresses = append(notReadyAddresses, *address)
					}
					continue
				}
				for _, containerStatus := range pod.Status.ContainerStatuses {
					if !containerStatus.Ready {
						toBeAdded = false
						log.Debugf("%s Service: %s Pod %s added to not ready addresses list", meth, name, pod.GetName())
						// Empty IPs are forbidden in Endpoints. So, we add the POD to the not
						// ready list only if it already has an assigned IP
						if address.IP != "" {
							notReadyAddresses = append(notReadyAddresses, *address)
						}
						break
					}
				}
			}
			if toBeAdded {
				log.Debugf("%s Service: %s Pod %s added to addresses list", meth, name, pod.GetName())
				addresses = append(addresses, *address)
			}
		}
		ports := make([]v1.EndpointPort, 0, len(service.Spec.Ports))
		// The ports in the Endpoints objects must have the same name than the ports
		// declated in the connector service.
		for _, servicePort := range service.Spec.Ports {
			port := v1.EndpointPort{
				Name:     servicePort.Name,
				Port:     context.Port,
				Protocol: servicePort.Protocol,
			}
			ports = append(ports, port)
		}
		if len(addresses) > 0 || len(notReadyAddresses) > 0 {
			subset := v1.EndpointSubset{
				Addresses:         addresses,
				NotReadyAddresses: notReadyAddresses,
				Ports:             ports,
			}
			subsets = append(subsets, subset)
		}
	}
	endpoints := &v1.Endpoints{
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Labels:      labels,
			Annotations: annotations,
		},
		Subsets: subsets,
	}

	return endpoints, nil
}

// createEndpointAddress creates a IPV4 endpoint address
func createEndpointAddress(pod *v1.Pod) *v1.EndpointAddress {
	return &v1.EndpointAddress{
		IP:       pod.Status.PodIP,
		NodeName: &pod.Spec.NodeName,
		TargetRef: &v1.ObjectReference{
			Kind:            "Pod",
			Namespace:       pod.ObjectMeta.Namespace,
			Name:            pod.ObjectMeta.Name,
			UID:             pod.ObjectMeta.UID,
			ResourceVersion: pod.ObjectMeta.ResourceVersion,
		}}
}
