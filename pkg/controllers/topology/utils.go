/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

import (
	"fmt"
	"sort"

	kerrors "topology-controller/pkg/utils/errors"

	v1 "k8s.io/api/core/v1"
)

// ShouldAbort returns true if the severity is not minor and false oherwirse
func ShouldSkip(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *kerrors.KukuError:
		kerr := err.(*kerrors.KukuError)
		if kerr.Severity == kerrors.Skip {
			return true
		}
		return false
	default:
		return false
	}
}

// ShouldAbort returns true if the severity is not minor and false oherwirse
func ShouldAbort(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *kerrors.KukuError:
		kerr := err.(*kerrors.KukuError)
		if kerr.Severity == kerrors.Minor {
			return false
		}
		return true
	default:
		return true
	}
}

// ShouldRetry returns false if the severity fatal and true otherwise
func ShouldRetry(err error) bool {
	switch err.(type) {
	case nil:
		return false
	case *kerrors.KukuError:
		kerr := err.(*kerrors.KukuError)
		if kerr.Severity == kerrors.Fatal || kerr.Severity == kerrors.Skip {
			return false
		}
		return true
	default:
		return true
	}
}

// ProcessError creates a new error composing an existing parent error and a new one.
func ProcessError(parent error, err error) error {
	switch parent.(type) {
	case nil:
		return err
	default:
		if err == nil {
			return nil
		}
		message := fmt.Sprintf("%s\n%s", parent.Error(), err.Error())
		return &kerrors.KukuError{Severity: kerrors.Minor, Parent: fmt.Errorf(message)}
	}
}

// AsKukuError checks if an error is a KukuError. In that case, returns
// the same error. Otherwise, returns a KukuError with the given severity and
// the original error as parent.
func AsKukuError(err error, severity kerrors.KukuErrorSeverity) *kerrors.KukuError {
	switch err.(type) {
	case *kerrors.KukuError:
		kerr := err.(*kerrors.KukuError)
		return kerr
	default:
		return &kerrors.KukuError{Severity: severity, Parent: err}
	}
}

// OrderPodsByName returns the same pods list but ordered by name
func OrderPodsByName(pods []*v1.Pod) []*v1.Pod {
	names := make([]string, 0, len(pods))
	orderedPods := make([]*v1.Pod, 0, len(pods))
	for _, pod := range pods {
		names = append(names, pod.GetName())
	}
	sort.Strings(names)
	for _, name := range names {
		for _, pod := range pods {
			if name == pod.GetName() {
				orderedPods = append(orderedPods, pod)
				break
			}
		}
	}
	return orderedPods
}

// SortPodsByName returns the same pods list but ordered by name
func SortPodsByName(items []*v1.Pod) []*v1.Pod {
	var num = len(items)

	if num == 1 {
		return items
	}

	middle := int(num / 2)
	var (
		left  = make([]*v1.Pod, middle)
		right = make([]*v1.Pod, num-middle)
	)
	for i := 0; i < num; i++ {
		if i < middle {
			left[i] = items[i]
		} else {
			right[i-middle] = items[i]
		}
	}

	return mergePods(SortPodsByName(left), SortPodsByName(right))
}

// mergePods is part of the pods name ordering algorithm
func mergePods(left, right []*v1.Pod) (result []*v1.Pod) {
	result = make([]*v1.Pod, len(left)+len(right))

	i := 0
	for len(left) > 0 && len(right) > 0 {
		if left[0].GetName() < right[0].GetName() {
			result[i] = left[0]
			left = left[1:]
		} else {
			result[i] = right[0]
			right = right[1:]
		}
		i++
	}

	for j := 0; j < len(left); j++ {
		result[i] = left[j]
		i++
	}
	for j := 0; j < len(right); j++ {
		result[i] = right[j]
		i++
	}

	return
}

// GetTaggedConnectorName returns the name associated to a connector/tag tuple
func GetTaggedConnectorName(connector, tag string) string {
	return fmt.Sprintf("%s-%s", connector, tag)
}
