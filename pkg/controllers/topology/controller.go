/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

// TODO:
// * Get KukuComponents and KukUServices
// * Create Service y NetworkPolicy
// * Apply updates
// * Restore associated kube elements when deleted
// * Use recorder to send events

import (
	"fmt"

	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	log "github.com/sirupsen/logrus"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	kubeclientset "k8s.io/client-go/kubernetes"

	v1 "k8s.io/api/core/v1"
	coreinformers "k8s.io/client-go/informers/core/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"

	"topology-controller/pkg/utils"
	endpointsManager "topology-controller/pkg/utils/kube/endpoints"
	serviceutils "topology-controller/pkg/utils/kube/services"
	linkutils "topology-controller/pkg/utils/kumori/kukulinks"
	v3dutils "topology-controller/pkg/utils/kumori/v3deployments"
	"topology-controller/pkg/utils/naming"
)

// Just the description-name of our controller
const controllerName = "topologycontroller"
const kindV3Depoloyment = "V3Deployment"

// Message constants used with events
// Note: It's ugly to use const for these definitions, but golang doesnt support
// "const structs". Alternative: https://github.com/Natata/enum
const (
	// ReasonResourceSynced is used as part of the Event 'reason' when a V3Deployment is synced
	ReasonResourceSynced = "Synced"

	// MessageResourceSynced is the message used for an Event fired when a Foo
	// is synced successfully
	MessageResourceSynced = "V3Deployment synced successfully"

	// ReasonErrResourceExists is used as part of the Event 'reason' when a V3Deployment fails
	// to sync due to a Deployment of the same name already existing.
	ReasonErrResourceExists = "ErrResourceExists"

	// MessageErrResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageErrResourceExists = "Resource %q already exists and is not managed by V3Deployment"
)

// Controller is the controller implementation for V3Deployments.
type Controller struct {
	base.BaseController

	kubeClientset   kubeclientset.Interface
	kumoriClientset kumoriclientset.Interface

	podInformer          coreinformers.PodInformer
	serviceInformer      coreinformers.ServiceInformer
	endpointsInformer    coreinformers.EndpointsInformer
	v3DeploymentInformer kumoriinformers.V3DeploymentInformer
	kukuLinkInformer     kumoriinformers.KukuLinkInformer

	podsLister          corelisters.PodLister
	servicesLister      corelisters.ServiceLister
	endpointsLister     corelisters.EndpointsLister
	v3DeploymentsLister kumorilisters.V3DeploymentLister
	kukuLinksLister     kumorilisters.KukuLinkLister

	kukuLinkTools linkutils.KukuLinkTools
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kumoriClientset kumoriclientset.Interface,
	podInformer coreinformers.PodInformer,
	serviceInformer coreinformers.ServiceInformer,
	endpointsInformer coreinformers.EndpointsInformer,
	v3DeploymentInformer kumoriinformers.V3DeploymentInformer,
	kukuLinkInformer kumoriinformers.KukuLinkInformer,
) (c *Controller) {
	log.Info("topology.Controller.NewController() Creating controller")

	syncedFunctions := make([]cache.InformerSynced, 5)
	syncedFunctions[0] = podInformer.Informer().HasSynced
	syncedFunctions[1] = serviceInformer.Informer().HasSynced
	syncedFunctions[2] = endpointsInformer.Informer().HasSynced
	syncedFunctions[3] = v3DeploymentInformer.Informer().HasSynced
	syncedFunctions[4] = kukuLinkInformer.Informer().HasSynced

	// This controller has not a primary object typel. Service is not a primary object
	// type because we are not interested in all services but only in the services related
	// to a V3Deployment connector. So, we don't want all Service objects to be queued
	// right away before any previous check.
	primarySharedInformers := make([]cache.SharedIndexInformer, 0)
	// primarySharedInformers[0] = serviceInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 5)
	secondarySharedInformers[0] = podInformer.Informer()
	secondarySharedInformers[1] = serviceInformer.Informer()
	secondarySharedInformers[2] = endpointsInformer.Informer()
	secondarySharedInformers[3] = kukuLinkInformer.Informer()
	secondarySharedInformers[4] = v3DeploymentInformer.Informer()

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     kindV3Depoloyment,
		Workqueue:                workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		Recorder:                 base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName),
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}

	c = &Controller{
		BaseController:       baseController,
		kubeClientset:        kubeClientset,
		kumoriClientset:      kumoriClientset,
		podInformer:          podInformer,
		serviceInformer:      serviceInformer,
		endpointsInformer:    endpointsInformer,
		v3DeploymentInformer: v3DeploymentInformer,
		kukuLinkInformer:     kukuLinkInformer,
		podsLister:           podInformer.Lister(),
		servicesLister:       serviceInformer.Lister(),
		endpointsLister:      endpointsInformer.Lister(),
		v3DeploymentsLister:  v3DeploymentInformer.Lister(),
		kukuLinksLister:      kukuLinkInformer.Lister(),
		kukuLinkTools:        linkutils.NewKukuLinkTools(v3DeploymentInformer.Lister(), kukuLinkInformer.Lister()),
	}

	c.BaseController.IBaseController = c

	return
}

// HandleSecondary is in charge of processing changes in the pods, services,
// endpoints, v3deployments or kukulinks. This controller does not have an
// explicit primary object but enqueues _Service_ objects representing
// V3Deployment objects connectors.
func (c *Controller) HandleSecondary(secondary interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Debugf(meth)
	obj, _, err := c.GetObject(secondary)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}
	switch obj := obj.(type) {

	// Looks for the related Service objects, which depend on the object type.
	case *kumoriv1.V3Deployment:
		// If it is a V3Deployment, enqueue all Service objects representing its
		// connectors.
		c.handleV3Deployment(obj)
		c.handleLinkedDeployments(obj, "", nil)
	case *v1.Service:
		// If it is a Service object, enqueue it if belongs to a V3Deployment
		// connector.
		c.handleService(obj)
	case *v1.Endpoints:
		// If it is an Endpoints object and it is paired to a V3Deployment connector,
		// enqueue the Service with the same name and namespace.
		c.handleEndpoints(obj)
	case *v1.Pod:
		// If it is a Pod and represents a V3Deployment role instance, find the
		// connectors this role is connected to through server and duplex channels
		// and enqueue their related Service objects.
		c.handlePod(obj)
	case *kumoriv1.KukuLink:
		// If it is a KukuLink, find the linked V3Deployment objects and enqueue
		// the Services related to their connectors.
		log.Infof("%s. Link handler %v", meth, obj.Spec.Endpoints)
		processed := make([]*kumoriv1.V3Deployment, 0, len(obj.Spec.Endpoints))
		for _, endpoint := range obj.Spec.Endpoints {
			if (endpoint.Kind == linkutils.DeploymentKukuLinkEndpointType) ||
				(endpoint.Kind == linkutils.SolutionKukuLinkEndpointType) {
				if v3Deployment, err := v3dutils.GetByRegistrationNameAndDomain(c.v3DeploymentsLister, obj.GetNamespace(), endpoint.Name, endpoint.Domain); err == nil {
					c.handleV3Deployment(v3Deployment)
					c.handleLinkedDeployments(v3Deployment, "", processed)
					processed = append(processed, v3Deployment)
					break
				} else {
					log.Errorf("%s. Error looking for V3Deployment '%s': %v", meth, endpoint.Name, err)
				}
			}
		}
	default:
		log.Errorf("%s. Unkwon object '%s' type. Skipping", meth, obj.GetName())
	}
}

// enqueueConnectorServices finds and enqueues the Service objects related
// to a given connector from a given deployment.
func (c *Controller) enqueueConnectorServices(
	connectorName string,
	connectorType kumoriv1.ConnectorType,
	deployment string,
	namespace string,
) {
	meth := fmt.Sprintf("%s.enqueueConnectorServices(). Connector: %s. Type: %s. Deployment: %s. Namespace: %s", c.Name, connectorName, connectorType, deployment, namespace)
	log.Debug(meth)

	// Get services related to this container and tag in the specified deployment
	matchLabels := map[string]string{
		KumoriConnectorLabel:    naming.Hash(connectorName),
		KumoriDeploymentIdLabel: deployment,
	}
	options := labels.SelectorFromSet(matchLabels)
	services, err := c.servicesLister.Services(namespace).List(options)
	if err != nil {
		log.Errorf("%s. Error found while retrieving services: %s", meth, err.Error())
		return
	}

	for _, service := range services {
		c.enqueueService(service)
	}
}

type endpoint struct {
	Name       string
	Domain     string
	Channel    string
	Deployment *kumoriv1.V3Deployment
}

func (c *Controller) handleLinkedDeployments(
	v3Deployment *kumoriv1.V3Deployment,
	channel string,
	skipV3Deployments []*kumoriv1.V3Deployment,
) {

	meth := fmt.Sprintf("%s.handleLinkedDeployments. V3Deployment: '%s/%s'. Channel: '%s'", c.Name, v3Deployment.GetNamespace(), v3Deployment.GetName(), channel)
	log.Debugf(meth)

	// Gets the names used when the deployment was registered by the service provider
	registeredName, ok := v3Deployment.Annotations[KumoriNameLabel]
	if !ok {
		log.Warnf("%s. V3Deployment has not a name label", meth)
		return
	}
	registeredDomain, ok := v3Deployment.Annotations[KumoriDomainLabel]
	if !ok {
		log.Warnf("%s. V3Deployment has not a domain label", meth)
		return
	}

	// This list contains the link endpoints pending to be followed. It is initially
	// filled with the provided deployment. Each endpoint is a deployment and a service channel
	// and indicates that we want to find out wich other endpoints we can reach following
	// the links affecting this deployment and service channel. If the service channel is the
	// empty string then we are insterested on following all links affecting the V3Deployment
	endpoints := []endpoint{{
		Name:       registeredName,
		Domain:     registeredDomain,
		Channel:    channel,
		Deployment: v3Deployment,
	}}

	// This is the list of endpoints we have already processed. It is used to avoid processing
	// the same endpoint twice.
	processedEndpoints := []kumoriv1.KukuEndpoint{}

	connectorsProcessed := map[string]*v3dutils.ConnectorInfo{}

	// Processes all endpoints. This list can grow but it is highly improbable. It will grow
	// if a given endpoint is linked to a service channel in another deployment which, at the
	// same time, is connected to a connector with another service channel connected.
	for len(endpoints) > 0 {

		// Pops the next endpoint
		currentEndpoint := endpoints[0]
		endpoints = endpoints[1:]

		// Gets the kukulinks linking this endpoint
		kukuLinks, err := c.kukuLinkTools.GetKukuLinks(currentEndpoint.Deployment, currentEndpoint.Channel)
		if err != nil {
			log.Errorf("%s. Links not found for endpoint Deployment: %s Channel: %s", meth, currentEndpoint.Deployment.Name, currentEndpoint.Channel)
			continue
		}

		// Follow each links to find out which V3Deployments are linked in the other side
		for _, link := range kukuLinks {

			// For each endpoint
			for _, linkEndpoint := range link.Spec.Endpoints {

				// Skips it if its not a deployment endpoint
				if (linkEndpoint.Kind != linkutils.DeploymentKukuLinkEndpointType) &&
					(linkEndpoint.Kind != linkutils.SolutionKukuLinkEndpointType) {
					continue
				}

				// Skips it if this is the endpoint being processed
				if (linkEndpoint.Domain == currentEndpoint.Domain) &&
					(linkEndpoint.Name == currentEndpoint.Name) &&
					((linkEndpoint.Channel == currentEndpoint.Channel) || (currentEndpoint.Channel == "")) {
					continue
				}

				// Skip this endpoint if belongs to a v3deployment to be skipped
				if skipV3Deployments != nil && len(skipV3Deployments) > 0 {
					for _, skipDeployment := range skipV3Deployments {
						skipName := skipDeployment.Annotations[KumoriNameLabel]
						skipDomain := skipDeployment.Annotations[KumoriDomainLabel]
						if (linkEndpoint.Domain == skipDomain) &&
							(linkEndpoint.Name == skipName) {
							continue
						}
					}
				}

				// Skips it if this endpoint has been already processed
				processed := false
				for _, processedEndpoint := range processedEndpoints {
					if (processedEndpoint.Domain == linkEndpoint.Domain) &&
						(processedEndpoint.Name == linkEndpoint.Name) &&
						(processedEndpoint.Channel == linkEndpoint.Channel) {
						processed = true
						break
					}
				}
				if processed {
					continue
				}

				// Adds the endpoint to the list of processed endpoints
				processedEndpoints = append(processedEndpoints, linkEndpoint)

				// Gets the endpoint V3Deployment using the name and domain
				newV3Deployment, err := v3dutils.GetByRegistrationNameAndDomain(c.v3DeploymentsLister, v3Deployment.Namespace, linkEndpoint.Name, linkEndpoint.Domain)
				if err != nil {
					log.Errorf("%s. Error looking for V3Deployment %s/%s in namespace %s: %s", meth, linkEndpoint.Domain, linkEndpoint.Name, v3Deployment.Namespace, err.Error())
					continue
				}
				if newV3Deployment == nil {
					log.Errorf("%s. V3Deployment %s/%s not found in namespace %s", meth, linkEndpoint.Domain, linkEndpoint.Name, v3Deployment.Namespace)
					continue
				}

				// Skips the V3Deployment if it is being deleted (has a deletion timestamp set)
				if !newV3Deployment.GetDeletionTimestamp().IsZero() {
					log.Debugf("%s. V3Deployment '%s/%s' has a deletion timestamp set. Skipping", meth, newV3Deployment.GetNamespace(), newV3Deployment.GetName())
					return
				}

				// Skips this V3Deployment if it has an empty connectors list
				if newV3Deployment.Spec.Artifact == nil ||
					newV3Deployment.Spec.Artifact.Description == nil ||
					newV3Deployment.Spec.Artifact.Description.Connectors == nil {
					log.Debugf("%s. V3Deployment '%s/%s' has an empty connectors list. Skipping", meth, newV3Deployment.GetNamespace(), newV3Deployment.GetName())
					continue
				}

				// Skips this V3Deployment if it is a built-in service
				if newV3Deployment.Spec.Artifact.Description.Builtin {
					log.Debugf("%s. V3Deployment '%s/%s' contains a built-in service. Skipping", meth, newV3Deployment.GetNamespace(), newV3Deployment.GetName())
					continue
				}

				// Gets the connector this endpoint is connected
				connectorInfo := v3dutils.GetChannelConnector(newV3Deployment, linkEndpoint.Channel, "self")

				// Skips this endpoint if it is not connected to any connector
				if connectorInfo == nil {
					log.Debugf("%s. Service channel '%s' not connected in V3Deployment '%s/%s'. Skipping", meth, linkEndpoint.Channel, newV3Deployment.GetNamespace(), newV3Deployment.GetName())
					continue
				}

				//Enqueue the connector services avoiding duplicates
				if connectorInfo.Connector.Servers != nil && len(connectorInfo.Connector.Servers) > 0 {
					key := fmt.Sprintf("%s-%s", newV3Deployment.Name, connectorInfo.Name)
					if _, ok := connectorsProcessed[key]; !ok {
						connectorsProcessed[key] = connectorInfo
						c.enqueueConnectorServices(connectorInfo.Name, connectorInfo.Connector.Kind, newV3Deployment.GetName(), newV3Deployment.GetNamespace())
					}
				}

				// Checks if connector has other service channels to follow (highly unlikely)
				if connectorInfo.AsClient {
					for _, serverData := range connectorInfo.Connector.Servers {
						for _, linkData := range serverData.Links {
							if linkData.Role == "self" {
								processed := false
								for _, processedEndpoint := range processedEndpoints {
									if processedEndpoint.Name == linkEndpoint.Name && processedEndpoint.Domain == linkEndpoint.Domain && processedEndpoint.Channel == linkData.Channel {
										processed = true
										break
									}
								}
								if !processed {
									endpoints = append(endpoints, endpoint{
										Name:       linkEndpoint.Name,
										Domain:     linkEndpoint.Domain,
										Channel:    linkData.Channel,
										Deployment: newV3Deployment,
									})
								}
							}
						}
					}
				} else {
					for _, clientData := range connectorInfo.Connector.Clients {
						if clientData.Role == "self" {
							processed := false
							for _, processedEndpoint := range processedEndpoints {
								if processedEndpoint.Name == linkEndpoint.Name && processedEndpoint.Domain == linkEndpoint.Domain && processedEndpoint.Channel == clientData.Channel {
									processed = true
									break
								}
							}
							if !processed {
								endpoints = append(endpoints, endpoint{
									Name:       linkEndpoint.Name,
									Domain:     linkEndpoint.Domain,
									Channel:    clientData.Channel,
									Deployment: newV3Deployment,
								})
							}
						}
					}
				}
			}
		}
	}
}

// handleV3Deployment enqueues the Service objects representing the connectors of a V3Deployment.
func (c *Controller) handleV3Deployment(v3Deployment *kumoriv1.V3Deployment) {
	deploymentName := v3Deployment.GetName()
	deploymentNamespace := v3Deployment.GetNamespace()
	meth := fmt.Sprintf("%s.HandleV3Deployment. V3Deployment: '%s/%s'", c.Name, deploymentNamespace, deploymentName)
	log.Debugf(meth)

	// Skip the V3Deployment if it is being deleted (has a deletion timestamp set)
	if !v3Deployment.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. V3Deployment has a deletion timestamp set. Skipping", meth)
		return
	}

	// gets V3Deployment connectors and queues its services
	if v3Deployment.Spec.Artifact == nil {
		log.Infof("%s. V3Deployment has an empty service artifact. Skipping", meth)
		return
	}
	if v3Deployment.Spec.Artifact.Description == nil {
		log.Infof("%s. V3Deployment has an empty service description. Skipping", meth)
		return
	}

	// Skips the V3Deployment if contains a built-in service
	if v3Deployment.Spec.Artifact.Description.Builtin {
		log.Infof("%s. V3Deployment has built-in service. Skipping", meth)
		return
	}

	if v3Deployment.Spec.Artifact.Description.Connectors != nil {
		connectors := *v3Deployment.Spec.Artifact.Description.Connectors
		for connectorName, connectorData := range connectors {
			if connectorData.Servers == nil || len(connectorData.Servers) <= 0 {
				continue
			}
			c.enqueueConnectorServices(connectorName, connectorData.Kind, deploymentName, deploymentNamespace)
		}
	}
}

// handleService enqueues a given service only if it is related to a V3Deployment connnector.
// It does that by inspecting the service and connector labels.
func (c *Controller) handleService(service *v1.Service) {
	serviceName := service.GetName()
	meth := fmt.Sprintf("%s.handleService(). Service: '%s/%s'", c.Name, service.GetNamespace(), serviceName)
	log.Debugf(meth)

	// If the Service is not going to be removed, queue it
	if !service.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. Service has a deletion timestamp set. Skipping", meth)
		return
	}

	_, ok := service.Labels[KumoriDeploymentIdLabel]
	if !ok {
		log.Debugf("%s. Service not belongs to a V3Deployment. Skipping", meth)
		return
	}
	_, ok = service.Annotations[KumoriConnectorLabel]
	if !ok {
		log.Debugf("%s. Service not belongs to a V3Deployment connector. Skipping", meth)
		return
	}
	c.enqueueService(service)
}

// handleEndpoints enqueues the service related to a given endpoint only if it belongs to a
// V3Deployment connector.
func (c *Controller) handleEndpoints(endpoints *v1.Endpoints) {
	endpointsName := endpoints.GetName()
	meth := fmt.Sprintf("%s.handleEndpoints(). Endpoints: '%s/%s'", c.Name, endpoints.GetNamespace(), endpointsName)
	log.Debugf(meth)

	_, ok := endpoints.Labels[KumoriDeploymentIdLabel]
	if !ok {
		log.Debugf("%s. Endpoints not belongs to a V3Deployment. Skipping", meth)
		return
	}
	namespace := endpoints.GetNamespace()
	service, err := serviceutils.GetByName(c.servicesLister, namespace, endpointsName)

	if err != nil {
		if errors.IsNotFound(err) {
			log.Warnf("%s. Service not found in namespace '%s'.", meth, namespace)
			return
		}
		log.Errorf("%s. Error retrieving Service '%s' in namespace '%s'. Error: %s.", meth, endpointsName, namespace, err.Error())
		return
	}
	c.enqueueService(service)

}

// handlePod enqueues the Services related to the connectors linked to a given POD role
// through server and duplex channels. The POD is discarded if it not belongs
// to a V3Deployment connector instance or its role does not has any server or
// duplex channels.
func (c *Controller) handlePod(pod *v1.Pod) {
	podName := pod.GetName()
	meth := fmt.Sprintf("%s.handlePod(). Pod: '%s/%s'", c.Name, pod.GetNamespace(), podName)
	log.Debugf(meth)

	// Checks this pod belongs to a V3Deployment. Skip otherwise.
	deploymentName, ok := pod.Labels[KumoriDeploymentIdLabel]
	if !ok {
		log.Debugf("%s. Pod not belongs to a V3Deployment. Skipping", meth)
		return
	}

	// Gets the POD role
	roleName, ok := pod.Annotations[KumoriRoleLabel]
	if !ok {
		log.Debugf("%s. Pod has not a role assigned. Skipping", meth)
		return
	}

	// Gets the pod V3Deployment
	namespace := pod.GetNamespace()
	// v3Deployment, err := c.v3DeploymentsLister.V3Deployments(namespace).Get(deploymentName)
	// v3Deployment = v3Deployment.DeepCopy()
	v3Deployment, err := v3dutils.GetByName(c.v3DeploymentsLister, namespace, deploymentName)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Warnf("%s. V3Deployment '%s' not found in namespace '%s'. Skipping", meth, deploymentName, namespace)
			return
		}
		log.Errorf("%s. Error retrieving V3Deployment '%s' in namespace '%s'. Error: %s. Skipping", meth, deploymentName, namespace, err.Error())
		return
	}

	// Skips the V3Deployment if it not contains an artifact or connectors
	if v3Deployment.Spec.Artifact == nil ||
		v3Deployment.Spec.Artifact.Description.Connectors == nil {
		log.Debugf("%s. V3Deployment contains an empty service or a service without connectors. Skipping", meth)
		return
	}

	// Skips the V3Deployment if contains a built-in service
	if v3Deployment.Spec.Artifact.Description.Builtin {
		log.Debugf("%s. V3Deployment contains a built-in service. Skipping", meth)
		return
	}

	// Gets the connectors linked this pod role
	connectorsInfo := v3dutils.GetRoleConnectors(v3Deployment, roleName)
	if (connectorsInfo == nil) || (len(connectorsInfo) == 0) {
		log.Warnf("%s. Role '%s' is not connected in V3Deployment '%s'. Namespace: '%s'. Skipping", meth, roleName, deploymentName, namespace)
		return
	}

	// Enqueues the services of connectors linking this POD role as a
	// server (the endpoints targets are PODs acting as servers). It also
	// gathers the service client channels linked to the affected connectors.
	// Those channels will be used later to enqueue also the affected serviced
	// from linked V3Deployments.
	serviceChannels := make([]string, 0, 5)
	for _, connectorInfo := range connectorsInfo {
		if connectorInfo.AsServer {

			// Enqueues the connectors services but only if this POD role is connected
			// as a server (the endpoints contain only the server channels)
			if connectorInfo.Connector.Servers != nil && len(connectorInfo.Connector.Servers) > 0 {
				c.enqueueConnectorServices(connectorInfo.Name, connectorInfo.Kind, v3Deployment.GetName(), v3Deployment.GetNamespace())
			}

			// Gets the client service channels to enqueue also the services of
			// linked V3Deployments
			if connectorInfo.Connector.Clients != nil && len(connectorInfo.Connector.Clients) > 0 {
				for _, clientData := range connectorInfo.Connector.Clients {
					if clientData.Role == "self" {
						alreadyAdded := false
						for _, ch := range serviceChannels {
							if ch == clientData.Channel {
								alreadyAdded = true
								break
							}
						}
						if !alreadyAdded {
							serviceChannels = append(serviceChannels, clientData.Channel)
						}
					}
				}
			}
		}
	}

	// Enqueue this V3Deployment
	c.handleV3Deployment(v3Deployment)

	// Enqueues the services of linked V3Deployments
	if len(serviceChannels) > 0 {
		log.Debugf("%s. Searching for links", meth)
		for _, channelName := range serviceChannels {
			c.handleLinkedDeployments(v3Deployment, channelName, nil)
		}
	} else {
		log.Debugf("%s. No links to search", meth)
	}

}

// enqueueService enqueues a Service only if it is not going to be deleted (if not has a
// deletion timestamp set)
func (c *Controller) enqueueService(service *v1.Service) {
	meth := fmt.Sprintf("%s.enqueueService(). Service: '%s'", c.Name, service.GetName())
	log.Debugf(meth)

	// This check is now performed when the context is loaded
	// // If the Service is not going to be removed, queue it
	// if service.GetDeletionTimestamp().IsZero() {
	// 	log.Debugf("%s enqueued", meth)
	// 	c.EnqueueObject(service)
	// 	return
	// }

	// // If the Service is going to be removed, skip it
	// log.Debugf("%s. Service has a deletion timestamp set. Skipping.", meth)

	log.Debugf("%s enqueued", meth)
	c.EnqueueObject(service)
}

// SyncHandler reconciles the actual state with the desired state
func (c *Controller) SyncHandler(key string) error {
	meth := fmt.Sprintf("%s.SyncHandler. Key: %s", c.Name, key)
	log.Infof(meth)

	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		log.Errorf("%s invalid resource", meth)
		return nil
	}
	log.Debugf("%s Name: %s Namespace: %s ", meth, name, namespace)

	// Get the required objects to calculate the expected Endpoints kubernetes objects
	context, err := CreateContext(namespace, name, c.v3DeploymentsLister, c.podsLister, c.servicesLister, c.kukuLinkTools)
	if ShouldSkip(err) {
		log.Debugf("%s. Skipping service due to: %s", meth, err.Error())
		return nil
	}
	if ShouldAbort(err) {
		if ShouldRetry(err) {
			log.Errorf("%s Recoverable error. Requeuing. Error: %s", meth, err.Error())
			return err
		}
		log.Errorf("%s Unrecoverable error. The process will be aborted and not retried. Error: %s", meth, err.Error())
		return nil
	}

	// Calculates the expected set of endpoints
	generator := NewEndpointsGenerator(context)
	endpoints, err := generator.Generate()
	if ShouldSkip(err) {
		log.Debugf("%s. Skipping service due to: %s", meth, err.Error())
		return nil
	}
	if ShouldAbort(err) {
		if ShouldRetry(err) {
			log.Errorf("%s Recoverable error. Requeuing. Error: %s", meth, err.Error())
			return err
		}
		log.Errorf("%s Unrecoverable error. The process will be aborted and not retried. Error: %s", meth, err.Error())
		return nil
	}

	// content, _ := json.MarshalIndent(endpoints, "", "  ")
	// fmt.Printf("\n\n------->ENDPOINTS: %s", string(content))

	// Checks if the service still exists to avoid ticket 1298
	service, err := serviceutils.Get(c.servicesLister, context.Service)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Infof("%s Skipping service due to: service not found. Parent: %v", meth, err.Error())
			return nil
		}
		log.Errorf("%s Recoverable error. Requeuing. Error: %s", meth, err.Error())
		return err
	}

	if !service.DeletionTimestamp.IsZero() {
		log.Infof("%s Skipping service due to: deletion timestamp set", meth)
		return nil
	}

	if service == nil {
		log.Infof("%s Skipping service due to: service not found", meth)
		return nil
	}

	// Apply the required updates
	endpoints, operator, err := endpointsManager.CreateOrUpdate(c.kubeClientset, c.endpointsLister, endpoints)
	if err != nil {
		log.Errorf("%s Recoverable error. Requeuing. Error: %s", meth, err.Error())
		return err
	}
	switch operator {
	case utils.CreatedOperation:
		log.Infof("%s. Endpoints %s created", meth, endpoints.GetName())
	case utils.UpdatedOperation:
		log.Infof("%s. Endpoints %s updated", meth, endpoints.GetName())
	case utils.DeletedOperation:
		log.Infof("%s. Endpoints %s deleted", meth, endpoints.GetName())
	case utils.NoneOperation:
		log.Infof("%s. Endpoints %s ok. Nothing to do", meth, endpoints.GetName())
	}

	return nil
}
