/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

import (
	"encoding/json"
	"fmt"
	kerrors "topology-controller/pkg/utils/errors"
	serviceutils "topology-controller/pkg/utils/kube/services"
	linkutils "topology-controller/pkg/utils/kumori/kukulinks"
	"topology-controller/pkg/utils/naming"

	log "github.com/sirupsen/logrus"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	corelisters "k8s.io/client-go/listers/core/v1"
)

const (
	// contextLogPrefix is used as a prefix in log lines
	contextLogPrefix = controllerName + ".ContextLoader"
)

// CreateContext creates and returns a context related to a given POD identified by its namespace and name
func CreateContext(
	namespace string,
	name string,
	deploymentLister kumorilisters.V3DeploymentLister,
	podLister corelisters.PodLister,
	serviceLister corelisters.ServiceLister,
	kukuLinkTools linkutils.KukuLinkTools,
) (*Context, error) {
	meth := contextLogPrefix + ".CreateContext()"
	log.Infof("%s Name: %s Namespace: %s", meth, name, namespace)

	// Creates a context loader containing the required listers to recover objects from the cluster state
	loader := &ContextLoader{
		DeploymentLister: deploymentLister,
		PodLister:        podLister,
		ServiceLister:    serviceLister,
		KukuLinkTools:    kukuLinkTools,
	}

	// Creates a context from the cluster state
	context, err := loader.load(namespace, name)
	if err != nil {
		return nil, err
	}

	// Returns the newly created context
	return context, nil
}

// load creates a new context and fills it with the cluster objects related to a given Service.
func (loader *ContextLoader) load(namespace string, name string) (*Context, error) {
	meth := fmt.Sprintf("%s.load(). Namespace: '%s'. Name: '%s'", contextLogPrefix, namespace, name)
	log.Info(meth)

	// Gets the Service references by the received namespace and name
	service, err := loader.getService(namespace, name)
	if err != nil {
		return nil, err
	}

	// Gets the Service V3Deployment name
	deploymentName, ok := service.ObjectMeta.Labels["kumori/deployment.id"]
	if !ok {
		newError := kerrors.KukuError{
			Parent:   fmt.Errorf("Service label not found in Service '%s.%s'", name, namespace),
			Severity: kerrors.Fatal,
		}
		return nil, &newError
	}

	// Gets the Service V3Deployment name
	connectorData, ok := service.ObjectMeta.Annotations["kumori/connector.data"]
	if !ok {
		newError := kerrors.KukuError{
			Parent:   fmt.Errorf("connector.data annotation not found in Service '%s.%s'", name, namespace),
			Severity: kerrors.Fatal,
		}
		return nil, &newError
	}

	// Gets the service connector data
	var connector ServiceConnectorAnnotation
	err = json.Unmarshal([]byte(connectorData), &connector)
	if err != nil {
		newError := kerrors.KukuError{
			Parent:   fmt.Errorf("wrong connector.data annotation found in Service '%s.%s'", name, namespace),
			Severity: kerrors.Fatal,
		}
		return nil, &newError
	}

	endpoints, err := loader.getConnectorEndpoints(deploymentName, namespace, &connector)
	if err != nil {
		newError := kerrors.KukuError{
			Parent:   err,
			Severity: kerrors.Major,
		}
		return nil, &newError
	}

	// Creates the context
	context := Context{
		Service:         service,
		Endpoints:       endpoints,
		PublishNonReady: service.Spec.PublishNotReadyAddresses,
	}

	return &context, nil
}

// getService gets a Service after a name and namespace
func (loader *ContextLoader) getService(namespace string, name string) (*v1.Service, error) {
	meth := fmt.Sprintf("%s.getService. Namespace: %s. Name: %s", contextLogPrefix, namespace, name)
	log.Debugf(meth)

	service, err := serviceutils.GetByName(loader.ServiceLister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Warnf("%s. Service not found", meth)
			return nil, &kerrors.KukuError{Severity: kerrors.Skip, Parent: err}
		}
		return nil, &kerrors.KukuError{Severity: kerrors.Major, Parent: err}
	}

	if !service.DeletionTimestamp.IsZero() {
		return nil, &kerrors.KukuError{Severity: kerrors.Skip, Parent: fmt.Errorf("deletion timestamp set")}
	}

	return service, nil
}

// getConnectorEndpoints returns the endpoints accepting connections through a given V3Deployment
// connector-tag tuple
func (loader *ContextLoader) getConnectorEndpoints(
	deploymentName string,
	namespace string,
	connector *ServiceConnectorAnnotation,
) ([]EndpointsContext, error) {
	meth := fmt.Sprintf("%s.getConnectorEndpoints(). Deployment: '%s'. Connector: '%s'. tag: '%s'", contextLogPrefix, connector.Deployment, connector.Name, connector.Tag)
	log.Debug(meth)

	endpoints := []EndpointsContext{}

	for _, serverList := range [][]ServiceConnectorAnnotationEndpoint{connector.Servers, connector.LinkedServers, connector.Duplex} {
		for _, server := range serverList {

			if server.Role != nil {

				// Calculates the endpoint deployment name
				deployment := deploymentName
				if server.Deployment != nil {
					deployment = *server.Deployment
				}

				port := int32(80)
				if server.Port != nil {
					port = *server.Port
				}

				// If this server channel belongs to a role, get that role PODs
				rolePods, err := loader.getRolePods(deployment, namespace, *server.Role)
				if err != nil {
					return nil, err
				}

				// Adds the new endpoint to the endpoints list
				endpoints = append(endpoints, EndpointsContext{
					Channel:    server.Channel,
					Role:       *server.Role,
					Deployment: deployment,
					Port:       port,
					Pods:       rolePods,
				})
			}

		}
	}

	// Returns the endpoints
	return endpoints, nil
}

// getRolePods returns the PODs running after given V3Deployment role
func (loader *ContextLoader) getRolePods(
	deployment string,
	namespace string,
	role string,
) ([]*v1.Pod, error) {
	meth := controllerName + ".ContextLoader.getRolePods()"
	log.Debugf("%s Deployment: %s Namespace: %s Role: %s", meth, deployment, namespace, role)

	matchLabels := map[string]string{
		"kumori/deployment.id": deployment,
		"kumori/role":          naming.Hash(role),
	}
	options := labels.SelectorFromSet(matchLabels)

	return loader.listPods(namespace, options)
}

// listPods gets PODs using a selector and a namespace
func (loader *ContextLoader) listPods(namespace string, selector labels.Selector) ([]*v1.Pod, error) {
	meth := contextLogPrefix + ".listPods()"
	log.Debugf("%s Namespace: %s Selector: %v", meth, namespace, selector)

	pods, err := loader.PodLister.Pods(namespace).List(selector)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Warnf("%s. PODs not found for namespace '%s' and selector '%v'.", meth, namespace, selector)
			return nil, &kerrors.KukuError{Severity: kerrors.Fatal, Parent: err}
		}
		return nil, &kerrors.KukuError{Severity: kerrors.Major, Parent: err}
	}

	cpods := make([]*v1.Pod, len(pods))
	for i, pod := range pods {
		cpods[i] = pod.DeepCopy()
	}
	cpods = OrderPodsByName(cpods)
	return cpods, nil
}
