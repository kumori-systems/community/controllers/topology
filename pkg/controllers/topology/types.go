/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

import (
	linkutils "topology-controller/pkg/utils/kumori/kukulinks"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	v1 "k8s.io/api/core/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
)

// ContextLoader can load a context
type ContextLoader struct {
	DeploymentLister kumorilisters.V3DeploymentLister
	PodLister        corelisters.PodLister
	ServiceLister    corelisters.ServiceLister
	KukuLinkTools    linkutils.KukuLinkTools
}

// Context contains all objects needed to calculate that expected Endpoints
type Context struct {
	Service         *v1.Service
	Endpoints       []EndpointsContext
	PublishNonReady bool
}

// EndpointsContext contains the information related to a connector endpoint, including the PODs manifest, the channel
// the connector is connected to and the port this channel binds to.
type EndpointsContext struct {
	Channel    string
	Role       string
	Deployment string
	Port       int32
	Pods       []*v1.Pod
}

// tagData contains information about a connector
type connectorData struct {
	Deployment *kumoriv1.V3Deployment
	Name       string
}

// tagData contains information about a connector tag
type tagData struct {
	Deployment *kumoriv1.V3Deployment
	Connector  string
	Tag        string
}

// ServiceConnectorAnnotation contains the structure of the JSON document stored in the
// `connector` annotation.
// * Name: the original connector name, without the tag
// * Tag: the tag name
// * Kind: the connector type ("full" or "lb")
// * Service: the connector service application name
// * Deployment: the connector deployment name
// * Clients: the client channels (role, channel)
// * Servers: the server channels (role, channel)
// * Duplex: the duplex channels (role, channel)
type ServiceConnectorAnnotation struct {
	Name          string                               `json:"name"`
	Tag           string                               `json:"tag"`
	Kind          string                               `json:"kind"`
	Service       string                               `json:"service"`
	Deployment    string                               `json:"deployment"`
	Clients       []ServiceConnectorAnnotationEndpoint `json:"clients"`
	Servers       []ServiceConnectorAnnotationEndpoint `json:"servers"`
	LinkedClients []ServiceConnectorAnnotationEndpoint `json:"linkedClients"`
	LinkedServers []ServiceConnectorAnnotationEndpoint `json:"linkedServers"`
	Duplex        []ServiceConnectorAnnotationEndpoint `json:"duplex"`
}

// ServiceConnectorAnnotationEndpoint contains information about a channel endpoint
type ServiceConnectorAnnotationEndpoint struct {
	Deployment *string `json:"deployment,omitempty"`
	Role       *string `json:"role,omitempty"`
	Channel    string  `json:"channel"`
	Port       *int32  `json:"port"`
}
