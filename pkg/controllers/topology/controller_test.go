/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package topology

import (
	"fmt"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/fake"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	utils "topology-controller/pkg/utils"

	// "k8s.io/client-go/informers/core/v1"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"

	// apiextensionsv1beta1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes/fake"
	cache "k8s.io/client-go/tools/cache"

	core "k8s.io/client-go/testing"
)

const threadiness int = 1
const ns string = "kumori"

type fixture struct {
	t *testing.T

	kubeClientset   *kubeclientset.Clientset
	kumoriClientset *kumoriclientset.Clientset
	// Objects to put in the store.
	pods          []*v1.Pod
	services      []*v1.Service
	v3Deployments []*kumoriv1.V3Deployment

	// Objects from here preloaded into NewSimpleFake.
	// TODO: no entiendo la diferencia entre la precarga y los objects añadidos al store.
	kubeobjects   []runtime.Object
	kumoriobjects []runtime.Object
	// Keys to be processed
	keys []string
}

func newFixture(t *testing.T) *fixture {
	f := &fixture{}
	f.t = t
	f.kumoriobjects = []runtime.Object{}
	f.kubeobjects = []runtime.Object{}
	f.pods = []*v1.Pod{}
	f.services = []*v1.Service{}
	f.v3Deployments = []*kumoriv1.V3Deployment{}
	return f
}

func (f *fixture) run() []core.Action {
	return f.runController()
}

func (f *fixture) runController() []core.Action {
	// Set up signals so we handle the shutdown signal gracefully
	stopCh := make(chan struct{})
	defer close(stopCh)

	resyncInterval := time.Duration(0)

	kubeClientset := kubeclientset.NewSimpleClientset(f.kubeobjects...)
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {},
	)

	kumoriClientset := kumoriclientset.NewSimpleClientset(f.kumoriobjects...)
	kumoriInformerFactory := kumoriinformers.NewFilteredSharedInformerFactory(
		kumoriClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {},
	)

	controller := NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Core().V1().Pods(),
		kubeInformerFactory.Core().V1().Services(),
		kubeInformerFactory.Core().V1().Endpoints(),
		kumoriInformerFactory.Kumori().V1().V3Deployments(),
		kumoriInformerFactory.Kumori().V1().KukuLinks(),
	)

	kubeInformerFactory.Start(stopCh)
	kumoriInformerFactory.Start(stopCh)

	time.Sleep(500 * time.Millisecond)

	for _, deployment := range f.v3Deployments {
		if err := kumoriInformerFactory.Kumori().V1().V3Deployments().Informer().GetIndexer().Add(deployment); err != nil {
			f.t.Fatal(err)
		}
	}

	for _, pod := range f.pods {
		if err := kubeInformerFactory.Core().V1().Pods().Informer().GetIndexer().Add(pod); err != nil {
			f.t.Fatal(err)
		}
	}

	for _, service := range f.services {
		if err := kubeInformerFactory.Core().V1().Services().Informer().GetIndexer().Add(service); err != nil {
			f.t.Fatal(err)
		}
	}

	errorCh := make(chan error)
	go controller.Run(1, stopCh, errorCh)

	for _, key := range f.keys {
		if err := controller.SyncHandler(key); err != nil {
			f.t.Fatal("main.main() Error running controller", err)
			return nil
		}
	}

	actions := kubeClientset.Actions()
	if actions == nil {
		f.t.Fatal("main.main(). Controller did nothing.")
		return nil
	}
	actions = filterInformerActions(actions)

	return actions

}

// filterInformerActions filters list and watch actions for testing resources.
// Since list and watch don't change resource state we can filter it to lower
// nose level in our tests.
func filterInformerActions(actions []core.Action) []core.Action {
	ret := []core.Action{}
	for _, action := range actions {
		if (action.GetNamespace() == ns) &&
			(action.Matches("list", "endpoints") ||
				action.Matches("watch", "endpoints")) {
			continue
		}
		ret = append(ret, action)
	}

	return ret
}

func (f *fixture) findResource(verb, resource, name, namespace string, actions []core.Action) metav1.Object {
	for _, action := range actions {
		if !action.Matches(verb, resource) {
			continue
		}
		obj, err := getActionObject(action)
		if err != nil {
			f.t.Errorf("%v", err)
			continue
		}

		if (obj.GetName() == name) && (obj.GetNamespace() == namespace) {
			return obj
		}

	}

	f.t.Fatalf("Action '%s' not found for resource '%s' in namespace '%s'", verb, name, namespace)
	return nil
}

func getActionObject(action core.Action) (metav1.Object, error) {
	var obj runtime.Object
	switch actionType := action.(type) {
	case core.CreateAction:
		createAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = createAction.GetObject()
	case core.UpdateAction:
		updateAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = updateAction.GetObject()
	case core.DeleteAction:
		deleteAction, ok := action.(core.CreateAction)
		if !ok {
			return nil, fmt.Errorf("Error processing action of type '%T'", actionType)
		}
		obj = deleteAction.GetObject()
	}

	if obj == nil {
		return nil, fmt.Errorf("Object not fount for action '%v'", action)
	}
	metaobj, err := getMetavObject(obj)
	if err != nil {
		return nil, err
	}
	return metaobj, nil

}

func getMetavObject(runObj runtime.Object) (metav1.Object, error) {
	switch objType := runObj.(type) {
	case *v1.Endpoints:
		return runObj.(metav1.Object), nil
	default:
		return nil, fmt.Errorf("Unknown type '%v'", objType)
	}
}

func (f *fixture) loadExample() {

	var worker1Pod v1.Pod
	var worker2Pod v1.Pod
	var frontend1Pod v1.Pod
	var serviceConnectorService v1.Service
	var serviceConnectorLBService v1.Service
	var lbConnectorService v1.Service
	var lbConnectorLBService v1.Service
	var fullConnectorService v1.Service
	var calculatorV3Deployment kumoriv1.V3Deployment

	err := utils.LoadObjectFromYAML(&worker1Pod, "../../../artifacts/examples/podWorker0.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&worker2Pod, "../../../artifacts/examples/podWorker1.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&frontend1Pod, "../../../artifacts/examples/podFrontend0.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&serviceConnectorService, "../../../artifacts/examples/serviceServiceConnector.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&serviceConnectorLBService, "../../../artifacts/examples/serviceServiceConnectorLb.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&lbConnectorService, "../../../artifacts/examples/serviceLbConnector.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&lbConnectorLBService, "../../../artifacts/examples/serviceLbConnectorLb.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&fullConnectorService, "../../../artifacts/examples/serviceFullConnector.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	err = utils.LoadObjectFromYAML(&calculatorV3Deployment, "../../../artifacts/examples/v3DeploymentCalculator.yaml")
	if err != nil {
		f.t.Errorf("%v", err)
	}

	f.pods = append(f.pods, &worker1Pod, &worker2Pod, &frontend1Pod)
	f.services = append(f.services, &serviceConnectorService, &serviceConnectorLBService, &lbConnectorService, &lbConnectorLBService, &fullConnectorService)
	f.v3Deployments = append(f.v3Deployments, &calculatorV3Deployment)
	f.kubeobjects = append(f.kubeobjects, &worker1Pod, &worker2Pod, &frontend1Pod, &serviceConnectorService, &serviceConnectorLBService, &lbConnectorService, &lbConnectorLBService, &fullConnectorService)
	f.kumoriobjects = append(f.kumoriobjects, &calculatorV3Deployment)

	// content, _ := json.MarshalIndent(f.services, "", "  ")
	// fmt.Printf("\n\n------->SERVICES: %s", string(content))
}

func TestController(t *testing.T) {

	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})

	log.SetLevel(log.DebugLevel)

	f := newFixture(t)

	f.loadExample()

	f.keys = []string{}
	for _, service := range f.services {
		key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(service)
		if err != nil {
			f.t.Fatal("main.main() Error running controller", err)
			return
		}
		f.keys = append(f.keys, key)
	}

	actions := f.run()

	// for _, action := range actions {
	// 	obj, err := getActionObject(action)
	// 	if err != nil {
	// 		t.Fatal(err)
	// 	}
	// 	fmt.Printf("--------------------------------> ACTIONS: %s\n", action.GetVerb())
	// 	fmt.Printf("                                  RESOURCE TYPE: %s\n", action.GetResource().Resource)
	// 	fmt.Printf("                                  TYPE: %s\n", reflect.TypeOf(action))
	// 	fmt.Printf("                                  NAME: %s\n", obj.GetName())
	// 	fmt.Printf("                                  NAMESPACE: %s\n", obj.GetNamespace())
	// }

	// EXPECTED ENDPOINTS
	f.findResource("create", "endpoints", "serviceconnector-0-kd-074937-c83ab019", ns, actions)
	f.findResource("create", "endpoints", "serviceconnector-0-kd-074937-c83ab019-lb", ns, actions)
	f.findResource("create", "endpoints", "serviceconnector-0-kd-074937-c83ab019-ready", ns, actions)
	f.findResource("create", "endpoints", "lbconnector-0-kd-074937-c83ab019", ns, actions)
	f.findResource("create", "endpoints", "lbconnector-0-kd-074937-c83ab019-lb", ns, actions)
	f.findResource("create", "endpoints", "lbconnector-0-kd-074937-c83ab019-ready", ns, actions)
	f.findResource("create", "endpoints", "fullconnector-0-kd-074937-c83ab019", ns, actions)
	f.findResource("create", "endpoints", "fullconnector-0-kd-074937-c83ab019-ready", ns, actions)
}
